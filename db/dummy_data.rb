Refinery::I18n.frontend_locales.each do |lang|
  I18n.locale = lang

  # Created Seeds
  def create_dummy_services
    services = {"Design": "<ul>
    <li>
      Complimentary in Your Home Consultation
    </li>
    <li>
      General Hourly Consultation Services Available
    </li>
    <li>
      Professional Space Planning Services
    </li>
    <li>
      Lovely Paint Color and Proper Paint Base Specifications
    </li>
    <li>
      Unique Floor, Wall, and Ceiling Treatments
    </li>
    <li>
      Millwork Plans including Windows, Doors, Trim, All Cabinetry, Built-Ins, Workstations, Wainscot, Closets, and More
    </li>
    <li>
      Fine Architectural Detailing
    </li>
    <li>
      Custom Fireplace Design
    </li>
    <li>
      Expert Kitchen, Bath, and Laundry Room Design including ADA and Aging-In-Place Solutions
    </li>
    <li>
      Plumbing Fixture and Countertop Selections
    </li>
    <li>
      Superior Lighting and Electrical Plans including all Lighting Fixture Selections
    </li>
    <li>
      Stunning Tile Work including Heated Tile Floors, Wall Tile, Decos, Trims, and Unique Patterns
    </li>
    <li>
      Architectural Hardware Selections including Door, Cabinet, and Decorative Bath Fixtures
    </li>
    <li>
      Simple to Full Blown Home Theatre Room Design
    </li>
    <li>
      Deployment of Smart Home Technologies
    </li>
    <li>
      Leveraging Healthy and Sustainable Materials
    </li>
    <li>
      Full Exterior Planning including Furnishings and Finish Selections
    </li>
    <li>
      Full AutoCAD Construction Documentation including Bid-Ready Blueprints with Key Notes and Schedules of All Materials
    </li>
  </ul>", "Build/Remodel": "<ul>
    <li>
      Architect, Landscape Architect, and Contractor Selections
    </li>
    <li>
      Bid Placement and Reviews
    </li>
    <li>
      Full Purchasing and Warehousing Capabilities
    </li>
    <li>
      Full Construction Management including:
    </li>
    <li>
      Take Off Meetings to Coordinate with All Contractors
    </li>
    <li>
      Create and Maintain Project Calendars
    </li>
    <li>
      Receive and Inspect Materials for Quality Assurance Prior to Install
    </li>
    <li>
      Provide Daily or As Needed On-site Inspections During Construction to Ensure Quality Execution
    </li>
    <li>
      Provide Punch List of All Final Work and Follow Through to 100% Completion
    </li>
    <li>
      Full Move-out and Move-in Services to Make Your Remodeling Experience Extra Pleasant
    </li>
    <li>
      Access to Highly Skilled and Qualified Artisan Tradespeople
    </li>
    <li>
      We Have Amazing Resources to Install Everything in a First Class Manner
    </li>
  </ul>", "Furnishing": "<ul>
    <li>
      Full Furnishing Selections
    </li>
    <li>
      Custom Furniture Design and Production
    </li>
    <li>
      Furniture Buying Trips
    </li>
    <li>
      Full Upholstery Workroom Services
    </li>
    <li>
      Full Soft Furnishing Workroom Services including Gorgeous Custom Draperies, Bedding, and More
    </li>
    <li>
      Unique Accessory Selections
    </li>
    <li>
      Reproduction and Fine Art Procurement
    </li>
    <li>
      Portable Lighting Selections
    </li>
    <li>
      Program and Hand Knot Area Rug Selections
    </li>
    <li>
      Antique Sourcing
    </li>
    <li>
      Fine Linens for Bed and Bath
    </li>
    <li>
      Tabletop Decor including Dinnerware, Flatware and More
    </li>
    <li>
      Interior Plantscaping
    </li>
    <li>
      Holiday Decor
    </li>
  </ul>"}

    services.each do |service, service_description|
      Refinery::Services::Service.create service_type: service, description: service_description
    end
  end

  create_dummy_services
  puts 'Refinery::Services dummy_data.rb ran successfully'
end
