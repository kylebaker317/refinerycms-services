
FactoryGirl.define do
  factory :service, :class => Refinery::Services::Service do
    sequence(:service_type) { |n| "refinery#{n}" }
  end
end

