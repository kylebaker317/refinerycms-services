# encoding: utf-8
require "spec_helper"

describe Refinery do
  describe "Services" do
    describe "Admin" do
      describe "services", type: :feature do
        refinery_login

        describe "services list" do
          before do
            FactoryGirl.create(:service, :service_type => "UniqueTitleOne")
            FactoryGirl.create(:service, :service_type => "UniqueTitleTwo")
          end

          it "shows two items" do
            visit refinery.services_admin_services_path
            expect(page).to have_content("UniqueTitleOne")
            expect(page).to have_content("UniqueTitleTwo")
          end
        end

        describe "create" do
          before do
            visit refinery.services_admin_services_path

            click_link "Add New Service"
          end

          context "valid data" do
            it "should succeed" do
              fill_in "Service Type", :with => "This is a test of the first string field"
              expect { click_button "Save" }.to change(Refinery::Services::Service, :count).from(0).to(1)

              expect(page).to have_content("'This is a test of the first string field' was successfully added.")
            end
          end

          context "invalid data" do
            it "should fail" do
              expect { click_button "Save" }.not_to change(Refinery::Services::Service, :count)

              expect(page).to have_content("Service Type can't be blank")
            end
          end

          context "duplicate" do
            before { FactoryGirl.create(:service, :service_type => "UniqueTitle") }

            it "should fail" do
              visit refinery.services_admin_services_path

              click_link "Add New Service"

              fill_in "Service Type", :with => "UniqueTitle"
              expect { click_button "Save" }.not_to change(Refinery::Services::Service, :count)

              expect(page).to have_content("There were problems")
            end
          end

        end

        describe "edit" do
          before { FactoryGirl.create(:service, :service_type => "A service_type") }

          it "should succeed" do
            visit refinery.services_admin_services_path

            within ".actions" do
              click_link "Edit this service"
            end

            fill_in "Service Type", :with => "A different service_type"
            click_button "Save"

            expect(page).to have_content("'A different service_type' was successfully updated.")
            expect(page).not_to have_content("A service_type")
          end
        end

        describe "destroy" do
          before { FactoryGirl.create(:service, :service_type => "UniqueTitleOne") }

          it "should succeed" do
            visit refinery.services_admin_services_path

            click_link "Remove this service forever"

            expect(page).to have_content("'UniqueTitleOne' was successfully removed.")
            expect(Refinery::Services::Service.count).to eq(0)
          end
        end

      end
    end
  end
end
