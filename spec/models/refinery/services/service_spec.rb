require 'spec_helper'

module Refinery
  module Services
    describe Service do
      describe "validations", type: :model do
        subject do
          FactoryGirl.create(:service,
          :service_type => "Refinery CMS")
        end

        it { should be_valid }
        its(:errors) { should be_empty }
        its(:service_type) { should == "Refinery CMS" }
      end
    end
  end
end
