module Refinery
  module Services
    module Admin
      class ServicesController < ::Refinery::AdminController

        crudify :'refinery/services/service',
                :title_attribute => 'service_type'

        private

        # Only allow a trusted parameter "white list" through.
        def service_params
          params.require(:service).permit(:service_type, :photo_id, :description)
        end
      end
    end
  end
end
